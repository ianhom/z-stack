//-------------------------------------------------------------------
// Filename: M270.c
// Description: hal M270 module library �~�q��/����(DI/DO)
//-------------------------------------------------------------------
//-------------------------------------------------------------------
// INCLUDES
//-------------------------------------------------------------------
#include "hal_defs.h"
#include "hal_mcu.h"
#include "hal_board.h"
#include "M270.h"

//-------------------------------------------------------------------
void M270_Init(void)
{
    HAL_IO_RELAY_OUTPUT();
}

//-------------------------------------------------------------------
void M270_SetDO(uint8 udo)
{
	if(udo == 0)
        {
          HAL_RELAY_OFF();
        }
	else if (udo == 1)
        {
          HAL_RELAY_ON();
        }
}
